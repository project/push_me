<?php

/**
 * @file
 */

/**
 * Implementation of hook_drush_command().
 */
function push_me_drush_command() {
  $items = [];

  $items['schedule-push-task'] = [
    'description' => 'Schedule task to send message to all users',
    'options' => [
      'message' => dt('Message to be sent'),
      'extra' => dt('JSON string of extra data'),
      'mode' => dt('Task processing mode'),
    ],
  ];

  $items['register-push-token'] = [
    'description' => 'Register new device with specified push token',
    'arguments' => [
      'fingerprint' => dt('Unique device identifier'),
      'os' => dt('Device OS'),
      'push_provider' => dt('Push provider'),
      'push_token' => dt('Push token'),
    ],
  ];

  return $items;
}

/**
 * Add task to send pushes to all users to the queue.
 */
function drush_push_me_schedule_push_task() {
  $message = drush_get_option('message', '');
  if (empty($message)) {
    drush_set_error(dt('Message text is required'));
    return;
  }

  $extra = drush_get_option('extra', []);
  $mode = drush_get_option('mode', PUSH_ME_TASK_MODE_IMMEDIATE);
  $data = [
    'message' => $message,
    'extra' => $extra,
    'payload' => '',
    'mode' => $mode,
  ];
  push_me_schedule_task($data);
}

/**
 * Register new device with specified push token.
 *
 * @param $fingerprint
 * @param $os
 * @param $push_provider
 * @param $push_token
 */
function drush_push_me_register_push_token($fingerprint, $os, $push_provider, $push_token) {
  $info = [
    'fingerprint' => $fingerprint,
    'os' => strtolower($os),
    'push_provider' => $push_provider,
    'push_token' => $push_token,
  ];

  try {
    push_me_register_device($info);
  }
  catch (Exception $e) {
    drush_set_error(dt($e->getMessage()));
  }
}
