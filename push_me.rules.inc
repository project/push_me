<?php

/**
 * @file
 * Rules integration for the Push Me module.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function push_me_rules_action_info() {
  $actions = [];

  $actions['push_me_schedule_task'] = [
    'label' => t('Send push message'),
    'parameter' => [
      'message' => [
        'type' => 'text',
        'label' => t('Message'),
        'translatable' => TRUE,
      ],
      'mode' => [
        'type' => 'text',
        'options list' => 'push_me_get_modes',
        'label' => t('When to send'),
        'translatable' => TRUE,
      ],
      'user' => [
        'type' => 'user',
        'label' => t('Recipient'),
        'optional' => TRUE,
        'translatable' => TRUE,
      ],
    ],
    'group' => t('Push me'),
    'callbacks' => [
      'execute' => 'push_me_schedule_task_action',
    ],
  ];

  return $actions;
}

/**
 * Action callback: Schedule a push messaging task.
 *
 * @param string $message
 * @param string $mode
 * @param object $user
 *
 * @throws RulesEvaluationException
 */
function push_me_schedule_task_action($message, $mode, $user = NULL) {
  // @TODO: Add selector for template with tokens
  // @TODO: Check events integration. Add rules integration (store disabled pre-configured rules per CT).
  $data = [
    'text' => $message,
    'extra' => [],
    'payload' => [],
    'settings' => [],
  ];
  $providers = [];
  $tokens = [];
  if (!empty($user)) {
    if ($device = _user_device_load(['uid' => $user->uid])) {
      $providers[] = $device->push_provider;
      $tokens[] = $device->push_token;
    }
    else {
      throw new RulesEvaluationException('User with uid %uid has no registered device', ['%uid' => $user->uid]);
    }
  }
  push_me_send_message($data, $mode, $providers, $tokens);
}

/**
 * Implements hook_rules_condition_info().
 *
 * @return array
 */
function push_me_rules_condition_info() {
  return [
    'push_me_push_notify_is_checked' => [
      'label' => t('"Notify all mobile users" is selected'),
      'parameter' => [
        'node' => ['label' => t('Node'), 'type' => 'node'],
      ],
      'group' => t('Push me'),
    ],
  ];
}

/**
 * Rules condition callback.
 *
 * @param $node
 *
 * @return bool
 */
function push_me_push_notify_is_checked($node) {
  return !empty($node->push_me['notify']);
}

/**
 * @}
 */
