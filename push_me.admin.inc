<?php

/**
 * @file
 *
 * Administrative forms and callbacks for Push Me module.
 */

use Drupal\push_me\PushProviderManager;

/**
 * Form callback for module settings.
 */
function push_me_settings_form($form, $form_state) {
  $types = [];
  foreach (node_type_get_types() as $type) {
    $types[$type->type] = t($type->name);
  }

  $form['allowed_notify_types'] = [
    '#type' => 'checkboxes',
    '#title' => t('Notify types'),
    '#options' => $types,
    '#multiple' => TRUE,
    '#default_value' => push_me_get_setting('allowed_notify_types', FALSE, FALSE, []),
    '#description' => t('Select content types to make possible notify about content update.'),
  ];

  $form['save'] = [
    '#type'  => 'submit',
    '#value' => t('Save'),
  ];

  return $form;
}

/**
 * Submit form handler.
 */
function push_me_settings_form_submit($form, &$form_state) {
  push_me_set_setting('allowed_notify_types', FALSE, FALSE, $form_state['values']['allowed_notify_types']);
}

/**
 * Form callback for sending a push message.
 */
function push_me_send_push_form($form, $form_state) {

  $form['message'] = [
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#default_value' => '',
  ];

  $options = push_me_get_modes();
  $form['task_mode'] = [
    '#type' => 'select',
    '#title' => t('Schedule'),
    '#options' => $options,
    '#default_value' => PUSH_ME_TASK_MODE_IMMEDIATE,
  ];

  $form['advanced'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  $form['advanced']['token'] = [
    '#type' => 'textarea',
    '#title' => t('Push tokens, comma-separated'),
    '#description' => t('In a mode other than "Right now" only tokens for existed devices will be pushed.'),
  ];

  $form['advanced']['extra'] = [
    '#type' => 'textarea',
    '#title' => t('Extra data JSON'),
    '#default_value' => '',
    '#description' => t('Example: @string', ['@string' => '{"params": {"type": "user"}}']),
  ];

  $form['advanced']['payload'] = [
    '#type' => 'textarea',
    '#title' => t('RAW message JSON'),
    '#default_value' => '',
    '#description' => t('Example: @string', ['@string' => '{"aps" : { "alert" : "Message received from Bob" }, "acme2" : [ "bang",  "whiz" ]}']),
  ];

  // @TODO: Exclude providers without settings (API KEY, certificates). Or use status report.
  $providers = ['' => t('All')] + PushProviderManager::getProvidersList(TRUE);
  $form['advanced']['provider'] = [
    '#type' => 'radios',
    '#title' => t('Push providers'),
    '#options' => $providers,
    '#default_value' => '',
  ];

  $form['advanced']['env_key'] = [
    '#type' => 'radios',
    '#title' => t('Certificate type'),
    '#options' => ['dev' => t('Dev'), 'prod' => t('Prod')],
    '#default_value' => push_me_get_setting('env_key', FALSE, FALSE, 'dev'),
    '#required' => TRUE,
  ];

  // @TODO: Use states to hide provider specific settings.
  $certificates = push_me_get_setting('certificates', PushProviderManager::PROVIDER_APNS, FALSE, []);
  if (!empty($certificates)) {
    $certificates = array_keys($certificates);
    $form['advanced']['certificate_set'] = [
      '#type' => 'radios',
      '#title' => t('Certificate set'),
      '#options' => array_combine($certificates, $certificates),
      '#default_value' => current($certificates),
      '#required' => TRUE,
    ];
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send'),
    '#weight' => 50,
  ];

  return $form;
}

/**
 * Validate form handler.
 */
function push_me_send_push_form_validate($form, &$form_state) {
  if (empty($form_state['values']['message']) && empty($form_state['values']['payload'])) {
    form_set_error('message', t('Message or payload is required'));
  }

  if (!empty($form_state['values']['token']) && empty($form_state['values']['provider'])) {
    form_set_error('token', t('If you specify token, you should choose provider'));
  }
}

/**
 * Submit form handler.
 */
function push_me_send_push_form_submit($form, &$form_state) {
  $task_mode = $form_state['values']['task_mode'];

  $settings = [
    'env_key' => $form_state['values']['env_key'],
  ];

  if (!empty($form_state['values']['certificate_set'])) {
    $settings['certificate_set'] = $form_state['values']['certificate_set'];
  }

  $data = [
    'text' => $form_state['values']['message'],
    'extra' => !empty($form_state['values']['extra']) ? json_decode($form_state['values']['extra'], TRUE) : [],
    'payload' => !empty($form_state['values']['payload']) ? json_decode($form_state['values']['payload'], TRUE) : [],
    'settings' => $settings,
  ];

  $providers = $form_state['values']['provider'] ? [$form_state['values']['provider']] : [];
  $tokens = $form_state['values']['token'] ? array_map('trim', explode(',', $form_state['values']['token'])) : [];
  $result = push_me_send_message($data, $task_mode, $providers, $tokens);

  // Show last inserted values on pushes form after each submit.
  $form_state['rebuild'] = TRUE;
}
