<?php
/**
 * @file
 *
 * PushMessage class.
 */

namespace Drupal\push_me;

/**
 * Platform agnostic container for a single message.
 *
 * @package Drupal\push_me
 */
class PushMessage {
  public $text = '';
  public $extraData = [];
  public $payload = [];

  public function __construct($text = '', array $extraData = [], array $payload = []) {
    $this->text = $text;
    $this->extraData = $extraData;
    $this->payload = $payload;
  }

  /**
   * @return string
   */
  public function __toString() {
    return $this->text . ':' . serialize($this->extraData);
  }

}
