<?php
/**
 * @file
 *
 * DeviceException class.
 */

namespace Drupal\push_me;

/**
 * Device related errors generic exception.
 *
 * @package Drupal\push_me
 */
class DeviceException extends \Exception {
  public function __construct($message = '', $code = 0) {
    parent::__construct($message, $code);
  }
}
