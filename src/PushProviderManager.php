<?php
/**
 * @file
 *
 * PushProviderManager class.
 */

namespace Drupal\push_me;

/**
 * Abstract factory for push providers.
 *
 * @package Drupal\push_me
 */
class PushProviderManager {

  const OS_GOOGLE = 'android';
  const OS_IOS = 'ios';

  const PROVIDER_APNS = 'apns';
  const PROVIDER_GCM = 'gcm';

  private static $providersMapping = [
    self::OS_GOOGLE => self::PROVIDER_GCM,
    self::OS_IOS => self::PROVIDER_APNS,
  ];

  public static function getSender($providerName, array $settings) {
    switch ($providerName) {
      case self::PROVIDER_APNS:
        return new ApnsPushSender($settings);

      case self::PROVIDER_GCM:
        return new GcmPushSender($settings);

      default:
        throw new PushSenderException('Missed implementation for provider: ' . $providerName);
    }
  }

  /**
   * Get default push provider based on OS name.
   *
   * @param $os
   *
   * @return string|FALSE
   */
  public static function getOsDefaultProvider($os) {
    return isset(self::$providersMapping[$os]) ? self::$providersMapping[$os] : FALSE;
  }

  public static function getProviderOs($provider) {
    $mapping = array_flip(self::$providersMapping);
    return isset($mapping[$provider]) ? $mapping[$provider] : FALSE;
  }

  public static function getProvidersList($getLabels = FALSE) {
    $providers = [
      self::PROVIDER_APNS => 'Apple Push notifications service',
      self::PROVIDER_GCM => 'Google cloud messaging',
    ];

    return $getLabels ? $providers : array_keys($providers);
  }
}
