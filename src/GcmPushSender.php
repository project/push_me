<?php
/**
 * @file
 *
 * GcmPushSender class.
 */

namespace Drupal\push_me;

/**
 * Sender for Google cloud messaging platform.
 *
 * @package Drupal\push_me
 */
class GcmPushSender extends PushSender {

  const GCM_SERVER_POST_URL = 'https://android.googleapis.com/gcm/send';
  const GCM_CHUNK_SIZE = 1000;
  private $headers = [];
  private $response;
  private $response_info = NULL;

  public function __construct(array $settings = []) {
    // Set up default headers.
    $headers = [];
    $headers[] = 'Content-Type:application/json';
    $headers[] = 'Authorization:key=' . $settings['api_key'];
    $this->headers = $headers;

    parent::__construct($settings);
  }

  private function buildPayload(PushMessage $message, $bundle_tokens) {
    if ($message->payload) {
      $payload = $message->payload;
    }
    else {
      // Messages which has no payload could not be empty.
      if (empty($message->text) && empty($message->extraData)) {
        return FALSE;
      }

      // Convert the payload into the correct format for GCM payloads.
      // Pre-fill an array with values from other modules first.
      $payload = [];
      // Fill the default values required for each payload.
      $payload['registration_ids'] = $bundle_tokens;
      $payload['collapse_key'] = (string) time();

      if (!empty($message->extraData)) {
        $payload['data'] = $message->extraData;
      }
      if (!empty($message->text)) {
        $payload['data']['message'] = $message->text;
      }
    }

    return json_encode($payload);
  }

  /**
   * @return \stdClass
   */
  private function getResponse() {
    return $this->response;
  }

  private function getResponseInfo() {
    return $this->response_info;
  }

  private function send($recipients, PushMessage $message) {
    $bundle_tokens = [];
    foreach ($recipients as $recipient) {
      $bundle_tokens[] = $recipient->push_token;
    }

    $payload = $this->buildPayload($message, $bundle_tokens);
    // If payload is invalid, go to next chunk.
    if (!$payload) {
      return;
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, self::GCM_SERVER_POST_URL);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    $response_raw = curl_exec($curl);
    $this->response_info = curl_getinfo($curl);
    curl_close($curl);

    $this->response = FALSE;
    if (isset($response_raw)) {
      $this->response = json_decode($response_raw);
    }
  }

  /**
   * @todo: We need to re-use last_id logic here to run chunks in external loop.
   *
   * @param array $recipients
   * @param PushMessage $message
   * @param null $last_id
   *
   * @return array|bool
   */
  public function sendMessage(array $recipients, PushMessage $message, &$last_id = NULL) {
    timer_start('send_push_message');

    // Define an array of result values.
    $result = [
      'count_attempted' => 0,
      'count_success' => 0,
      'success' => 0,
      'message' => '',
    ];

    // Check of many token bundles can be build.
    $token_bundles = ceil(count($recipients) / self::GCM_CHUNK_SIZE);
    $result['count_attempted'] = count($recipients);

    // Send a push notification to every recipient.
    for ($i = 0; $i < $token_bundles; $i++) {
      // Create a token bundle.
      $recipients_chunk = array_slice($recipients, $i * self::GCM_CHUNK_SIZE, self::GCM_CHUNK_SIZE, FALSE);
      $this->send($recipients_chunk, $message);

      $response = $this->getResponse();
      $info = $this->getResponseInfo();
      // If Google's server returns a reply, but that reply includes an error,
      // log the error message.
      if ($info['http_code'] == 200 && (!empty($response->failure))) {
        // Analyze the failure.
        foreach ($response->results as $token_index => $message_result) {
          if (!empty($message_result->error)) {
            // If the device token is invalid or not registered (anymore because the user
            // has uninstalled the application), remove this device token.
            if ($message_result->error == 'NotRegistered' || $message_result->error == 'InvalidRegistration') {
              // @todo: Add purge logic into core.
              //$recipient_index = ($i * self::GCM_CHUNK_SIZE) + $token_index;
              //api_device_purge_push_token('android', $recipients[$recipient_index]->push_token);
            }
          }
        }
      }

      // Count the successful sent push notifications if there are any.
      if ($info['http_code'] == 200 && !empty($response->success)) {
        $result['count_success'] += $response->success;
      }
    }

    $result['success'] = TRUE;
    $result['stats']['time_lapsed'] = timer_read('send_push_message');
    $result['stats']['memory'] = memory_get_peak_usage();
    return $result;
  }

}
