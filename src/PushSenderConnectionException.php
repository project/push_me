<?php
/**
 * @file
 *
 * PushSenderConnectionException class.
 */

namespace Drupal\push_me;

/**
 * Generic exception for any connection error.
 *
 * @package Drupal\push_me
 */
class PushSenderConnectionException extends PushSenderException {

}
