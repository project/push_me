<?php
/**
 * @file
 *
 * PushSender class.
 */

namespace Drupal\push_me;

/**
 * Basic class to contain common logic for any push provider server.
 *
 * @package Drupal\push_me
 */
abstract class PushSender {
  const PAYLOAD_LIMIT_REACHED_ERROR_CODE = 1001;
//  const RECIPIENT_ATTEMPTS_LIMIT = 3;
  const PAYLOAD_LIMIT = 255;
  const LOG_DIR = 'private://push-messages-log';
  protected $settings = array();
  protected static $errors = array();

  public function __construct(array $settings = array()) {
    $this->settings = $settings;

    if (!isset($this->settings['test_mode'])) {
      $this->settings['test_mode'] = FALSE;
    }

    // Clean previous times.
    if (isset($GLOBALS['timers']['send_push_message'])) {
      unset($GLOBALS['timers']['send_push_message']);
    }
  }

  /**
   * Check size of a notification payload.
   *
   * @param $payload
   * @return bool
   */
  public function payloadIsValid($payload) {
    return !empty($payload) && mb_strlen($payload, '8bit') <= self::PAYLOAD_LIMIT;
  }

  public static function buildRecipient($id, $pushToken) {
    $recipient = new \stdClass();
    $recipient->id = $id;
    $recipient->push_token = $pushToken;

    return $recipient;
  }

  public function sendMessage(array $recipients, PushMessage $message, &$last_id = NULL) {

  }

  public static function errorLabel($code) {
    return isset(self::$errors[$code]) ? self::$errors[$code] : $code;
  }
}
