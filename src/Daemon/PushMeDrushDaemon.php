<?php

/**
 * @file
 * PushMeDrushDaemon class
 */

use Drupal\push_me\PushTaskProcessor;

/**
 * Implementation of drush daemon for background message processing.
 */
class PushMeDrushDaemon extends \DrushDaemon {

  protected $loop_interval = 350;

  protected $auto_restart_interval = 3600;

  public function runTask() {
    $this->executeTask(FALSE);
  }

  protected function executeTask($iteration_number) {
    // Run single iteration of messages processing.
    $processor = new PushTaskProcessor();
    try {
      $processor->process();
    }
    catch (Exception $e) {
      $this->log($e->getMessage());
    }
  }

}
