<?php
/**
 * @file
 *
 * PushSenderException class.
 */

namespace Drupal\push_me;

/**
 * Custom generic exception.
 *
 * @package Drupal\push_me
 */
class PushSenderException extends \Exception {

  public function __construct($message = '', $code = 0) {
    parent::__construct($message, $code);
  }

}
