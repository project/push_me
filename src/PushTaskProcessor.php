<?php
/**
 * @file
 *
 * PushTaskProcessor class
 */

namespace Drupal\push_me;

/**
 * Main logic of bulk messages processing.
 *
 * @package Drupal\push_me
 */
class PushTaskProcessor {

  protected $messages_limit = 1000;

  protected $reporting = TRUE;

  private $time_speed = FALSE;

  private $task;

  public static function getRawInstance() {
    return new static;
  }

  public function setMessagesLimit($limit) {
    $this->messages_limit = $limit;
  }

  public function setTimeSpeed($time_speed) {
    $this->time_speed = $time_speed;
  }

  private function report($task, $log_type, $params = []) {
    if (!$this->reporting) {
      return;
    }

    $log_file = 'task-' . $task->tid . '.txt';
    $log_dir = PushSender::LOG_DIR . '/' . date('Y', $task->created) . '/' . date('m', $task->created);
    if (!file_prepare_directory($log_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      throw new PushTaskProcessorException('Failed to write push messages report.');
    }

    $messages = [
      'started' => 'Task ID:@id started at @time.',
      'competed' => 'Task ID:@id completed at @time.',
      'hour_end' => 'Hour zone @hour finished. Unprocessed @unprocessed recipients.',
      'chunk_send' => 'Chunk completed in @time_lapsed ms (@memory).' . PHP_EOL .
        'Status: @status' . PHP_EOL .
        'Total items: @total, prepared: @messages_prepared, sent: @messages_sent.' . PHP_EOL .
        'Reconnections: @reconnection_count, Last ID: @last_id' . PHP_EOL .
        'Extra data: "@extra"' . PHP_EOL .
        'Payload: "@payload"' . PHP_EOL .
        'Result message: "@error_message"',
      'chunk_send_error' => 'Error @error encountered @count times',
      'chunk_send_payload_error' => 'Payload limit error for payload: @payload',
    ];

    $params += ['@time' => format_date(time(), 'custom', 'd/m/Y H:i:s'), '@id' => $task->tid];

    $log_entry = '------------------------------' . PHP_EOL;
    $log_entry .= format_string($messages[$log_type], $params) . PHP_EOL;

    file_put_contents($log_dir . '/' . $log_file, $log_entry, FILE_APPEND);
  }

  /**
   * @TODO: Respect disabled push-users and don't send – add column with index, store flag.
   * @TODO: Should we move this to another module (user_device)?
   *
   * @param $task
   * @param $hour
   * @param $last_id
   *
   * @return \SelectQuery
   */
  private function buildQuery($task, $hour, $last_id) {
    $query = db_select('users_devices', 'd');
    $query->fields('d');
    $query->addField('d', 'did', 'id');
    $query->isNotNull('d.push_token');
    $query->condition('d.did', $last_id, '>');
    // Immediate tasks should not be filtered by hour.
    if ($hour !== -1) {
      $query->condition('d.hour', $hour);
    }
    // @todo: Add group by push token - it should be unique?
    $query->range(0, $this->messages_limit);
    if (!empty($task->data['filters'])) {
      foreach ($task->data['filters'] as $field => $value) {
        $query->condition('d.' . $field, $value);
      }
    }

    return $query;
  }

  /**
   * We need to control flow with own time to speed up processing of full cycle.
   */
  public function time() {
    static $start_point = 0;

    if ($this->time_speed) {
      if (!$start_point) {
        $start_point = time();
        $diff = 0;
      }
      else {
        $diff = time() - $start_point;
      }

      return $start_point + ($diff * $this->time_speed);
    }
    else {
      return time();
    }
  }

  public function setPushTask($task) {
    $this->task = $task;
  }

  private function getPushTask() {
    return $this->task ? $this->task : \PersistentTask::getNext();
  }

  protected function processTask($task) {
    $completed = FALSE;
    if ($task->data['mode'] == PUSH_ME_TASK_MODE_HOURLY) {
      // Complete any task longer then one day.
      $completed = ($this->time() - $task->started) >= 24 * 60 * 60;
      // Check if hour zone is changed - log how much items were not processed.
      $current_hour = intval(date('H', $this->time()));
      if ((isset($task->data['hour']) && $task->data['hour'] != $current_hour) || $completed) {
        if ((isset($task->data['hour']) && $task->data['hour'] != $current_hour)) {
          $hour_to_log = $current_hour === 0 ? 23 : $current_hour - 1;
        }
        else {
          $hour_to_log = $current_hour;
        }
        $log_last_id = isset($task->data['last_id'][$hour_to_log]) ? $task->data['last_id'][$hour_to_log] : 0;
        $query = $this->buildQuery($task, $hour_to_log, $log_last_id);
        $num_rows = $query->countQuery()->execute()->fetchField();

        $this->report($task, 'hour_end', ['@hour' => $hour_to_log, '@unprocessed' => $num_rows]);
      }
    }
    else {
      $current_hour = -1;
    }

    // Once we have completed flag - task is done, do not send anything.
    if (!$completed) {
      $last_id = isset($task->data['last_id'][$current_hour]) ? $task->data['last_id'][$current_hour] : 0;
      $query = $this->buildQuery($task, $current_hour, $last_id);
      $recipients = $query->execute()->fetchAllAssoc('id');

      // For immediate mode if rows count less than limit then task is completed.
      if ($task->data['mode'] == PUSH_ME_TASK_MODE_IMMEDIATE) {
        $completed = count($recipients) < $this->messages_limit;
      }

      if (!empty($recipients)) {
        $provider = $task->data['filters']['push_provider'];
        $settings = isset($task->data['settings']) ? $task->data['settings'] : [];
        $settings = push_me_add_default_settings($provider, $settings);

        // @TODO: Integrate here support of templates per task type.
        $text = !empty($task->data['text']) ? $task->data['text'] : FALSE;
        // Add debug info for testing.
        if ($text && $settings['env_key'] == 'dev') {
          $text = '[' . $task->tid . ':' . $current_hour . ']' . $text;
        }

        $extra = !empty($task->data['extra']) ? $task->data['extra'] : [];
        $payload = !empty($task->data['payload']) ? $task->data['payload'] : [];
        $message = new PushMessage($text, $extra, $payload);

        $sender = PushProviderManager::getSender($provider, $settings);
        $result = $sender->sendMessage($recipients, $message, $last_id);

        if ($this->reporting) {
          $log_data = [];
          $log_data['@total'] = count($recipients);
          $log_data['@status'] = $result['success'] ? 'Completed' : 'Failed';
          $log_data['@last_id'] = $last_id;
          $log_data['@extra'] = serialize($message->extraData);
          $log_data['@payload'] = serialize($message->payload);
          if (!empty($result['message'])) {
            $log_data['@error_message'] = $result['message'];
          }
          else {
            $log_data['@error_message'] = 'None';
          }
          if (isset($result['stats'])) {
            foreach ($result['stats'] as $key => $value) {
              $log_data['@' . $key] = $value;
            }
          }
          $log_data['@memory'] = format_size($log_data['@memory']);
          $this->report($task, 'chunk_send', $log_data);


          $resent_count = 0;
          $encountered_errors = [];
          foreach ($recipients as $recipient) {
            if (isset($recipient->attempts_count) && $recipient->attempts_count > 1) {
              $resent_count++;
            }

            if (!empty($recipient->error_code)) {
              if ($recipient->error_code == PushSender::PAYLOAD_LIMIT_REACHED_ERROR_CODE) {
                $this->report($task, 'chunk_send_payload_error', ['@payload' => $recipient->payload]);
              }
              else {
                if (!isset($encountered_errors[$recipient->error_code])) {
                  $encountered_errors[$recipient->error_code] = 0;
                }
                $encountered_errors[$recipient->error_code]++;
              }
            }
          }

          if ($resent_count) {
            $this->report($task, 'chunk_send_error', ['@error' => 'Item resent', '@count' => $resent_count]);
          }

          foreach ($encountered_errors as $error_code => $count) {
            $this->report($task, 'chunk_send_error', ['@error' => PushSender::errorLabel($error_code), '@count' => $count]);
          }
        }


        $task->data['last_id'][$current_hour] = $last_id;
      }

      // Always update hour of the last run.
      $task->data['hour'] = $current_hour;
    }

    return $completed;
  }

  /**
   * Find next task to process and run one iteration.
   *
   * @throws \Drupal\push_me\PushSenderException
   * @throws \Drupal\push_me\PushTaskProcessorException
   */
  public function process() {
    if ($task = $this->getPushTask()) {
      if (!$task->started) {
        $task->started = $this->time();
      }

      if ($task->status == \PersistentTask::STATUS_NEW) {
        $task->status = \PersistentTask::STATUS_IN_PROGRESS;
        $this->report($task, 'started');
      }

      $completed = $this->processTask($task);

      if ($completed) {
        $task->status = \PersistentTask::STATUS_COMPLETED;
        $task->completed = $this->time();
        $this->report($task, 'competed');
      }

      \PersistentTask::update($task);
    }
  }

}
