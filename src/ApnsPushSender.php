<?php
/**
 * @file
 *
 * ApnsPushSender class.
 */

namespace Drupal\push_me;

/**
 * Sender for Apple push notifications server (APNS).
 *
 * Implementation of push messaging with APNS.
 * Good example — http://stackoverflow.com/questions/10058768/php-apple-enhanced-push-notification-read-error-response
 *
 * @package Drupal\push_me
 */
class ApnsPushSender extends PushSender {
  private $connection = NULL;

  const APNS_PORT = 2195;
  const PUSH_TOKEN_LENGTH = 64;
  const SUCCESS_CODE = 0;
  const SHUTDOWN_CODE = 10;

  // Do not store on APNS and send immediately.
  private $expiry = 0;
  protected static $errors = [
    '0' => 'No errors encountered',
    '1' => 'Processing error',
    '2' => 'Missing device token',
    '3' => 'Missing topic',
    '4' => 'Missing payload',
    '5' => 'Invalid token size',
    '6' => 'Invalid topic size',
    '7' => 'Invalid payload size',
    '8' => 'Invalid token',
    '10' => 'Shutdown',
    '255' => 'None (unknown)',
  ];

  private function hasConnection() {
    return (bool) $this->connection;
  }

  /**
   * Open an APNS connection.
   * Should be closed by calling fclose($connection) after usage.
   */
  private function connect() {
    // Test mode used to debug sender without real messages sent.
    // All output goes into temp file.
    if ($this->settings['test_mode']) {
      $connection = fopen(self::LOG_DIR . '/stream-mock.txt', 'w+');
    }
    else {
      // Allow key to be overridden on form.
      $env_key = $this->settings['env_key'];
      $certificate_set = !empty($this->settings['certificate_set']) ? $this->settings['certificate_set'] : 'default';

      // Determine the absolute path of the certificate.
      // @see http://stackoverflow.com/questions/809682
      $certificate_file = $this->settings['certificates'][$certificate_set][$env_key];
      // @TODO: Find good solution to store certificates.
      $apns_cert = drupal_realpath(DRUPAL_ROOT . '/' . drupal_get_path('module', 'push_me') . '/certificates/' . $certificate_file);

      // Create a stream context.
      $stream_context = stream_context_create();
      // Set options on the stream context.
      stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);

      // Open an Internet socket connection.
      $connection = stream_socket_client(
        'ssl://' . $this->settings['host'][$env_key] . ':' . self::APNS_PORT,
        $error,
        $error_string,
        2,
        STREAM_CLIENT_CONNECT,
        $stream_context
      );

      if ($connection) {
        //This allows fread() to return right away when there are no errors.
        // But it can also miss errors during last seconds of sending, as there is a delay before error is returned.
        // Workaround is to pause briefly AFTER sending last notification, and then do one more fread() to see if anything else is there.
        stream_set_blocking($connection, 0);
      }
    }

    if (!$connection) {
      throw new \Exception('APNS Connection failed');
    }

    return $connection;
  }

  private function setupConnection() {
    if (!$this->hasConnection()) {
      try {
        $this->connection = $this->connect();
      } catch (\Exception $e) {
        // Terminate any actions if connect failed.
        throw new PushSenderConnectionException('APNS Connection failed');
      }
    }

    return $this->connection;
  }

  private function closeConnection() {
    if ($this->hasConnection()) {
      try {
        fclose($this->connection);
        $this->connection = NULL;
      } catch (\Exception $e) {
        throw new PushSenderConnectionException('Failed to close connection');
      }
    }
  }

  private function send($recipient, PushMessage $message) {
    // Don't prepare payload again for already processed item or pre-defined data.
    if (!isset($recipient->payload)) {
      // Store payload even if it's invalid to log it later.
      $recipient->payload = $this->buildPayload($message);
    }

    if (!$this->payloadIsValid($recipient->payload)) {
      $recipient->error_code = self::PAYLOAD_LIMIT_REACHED_ERROR_CODE;
      return FALSE;
    }

    $expiry = time() + (90 * 24 * 60 * 60);
    return fwrite(
      $this->connection,
      $this->formatRequest($recipient->id, $expiry, $recipient->push_token, $recipient->payload)
    );
  }

  /**
   * Format message data as payload to transfer.
   *
   * @param PushMessage $message
   *
   * @return bool|string
   */
  private function buildPayload(PushMessage $message) {
    if ($message->payload) {
      $payload = $message->payload;
    }
    else {
      // Messages which has no payload could not be empty.
      if (empty($message->text) && empty($message->extraData)) {
        return FALSE;
      }

      $payload = $message->extraData;
      $payload['aps'] = [
        // @todo: We need to add it via extra params of certain push type.
        // We always send +1 badge counter.
        'badge' => 1
      ];
      if (!empty($message->text)) {
        $payload['aps']['alert'] = $message->text;
      }
    }

    return json_encode($payload);
  }

  /**
   * Format binary value.
   *
   * @param $id
   * @param $expiry
   * @param $token
   * @param $payload
   * @return string
   */
  private function formatRequest($id, $expiry, $token, $payload) {
    return pack("C", 1) . pack("L", $id) . pack("L", $expiry) . pack("n", 32) . pack('H*', $token) . pack("n", mb_strlen($payload, '8bit')) . $payload;
  }

  private function getResponse() {
    // We need to make a time-out, see connect() + stream_set_blocking().
    usleep(500000);
    $apple_error_response = fread($this->connection, 6);
    // NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.
    // unpack the error response (first byte 'command" should always be 8)
    return $apple_error_response ? unpack('Ccommand/Cstatus_code/Lidentifier', $apple_error_response) : FALSE;
  }

  /**
   * Callback to process next batch of messages.
   * @todo: Figure out in future how to send messages in parallel streams.
   * @todo: Integrate recent Apple messaging system https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/CommunicatingWIthAPS.html
   *
   * @param array $recipients
   * @param PushMessage $message
   * @param null $last_id
   *
   * @return array
   *
   * @throws PushSenderConnectionException
   */
  public function sendMessage(array $recipients, PushMessage $message, &$last_id = NULL) {
    timer_start('send_push_message');

    $result = ['success' => TRUE];
    $messages_prepared = 0;
    $messages_sent = 0;
    $reconnection_count = 0;

    try {
      $unprocessed_recipients = $recipients;
      // We are using indexes to rewind by index in case of error.
      $indexes = [];
      foreach ($recipients as $recipient) {
        $indexes[] = $recipient->id;
      }
      while (!empty($unprocessed_recipients)) {
        while ($recipient = array_shift($unprocessed_recipients)) {
          if (!$this->hasConnection()) {
            $this->setupConnection();
            $reconnection_count++;
          }

          $last_id = $recipient->id;
          if (function_exists('dsm')) {
            dsm('ID:' . $last_id);
          }

          $recipient->attempts_count = isset($recipient->attempts_count) ? $recipient->attempts_count + 1 : 1;

          // Avoid stack on same item for unpredicted error cases.
//          if ($recipient->attempts_count > self::RECIPIENT_ATTEMPTS_LIMIT) {
//            continue;
//          }

          if (mb_strlen($recipient->push_token, '8bit') != self::PUSH_TOKEN_LENGTH) {
            $recipient->error_code = 5;
            continue;
          }

          $messages_prepared++;

          if ($this->send($recipient, $message)) {
            $messages_sent++;
          }
        }

        $response = $this->getResponse();
        if (function_exists('dsm')) {
          dsm('Response');
          dsm($response);
        }
        // The packet has a command value of 8 followed by a one-byte status code and the notification identifier of the malformed notification. Table 5-1 lists the possible status codes and their meanings.
        // A status code of 10 indicates that the APNs server closed the connection (for example, to perform maintenance).
        // The notification identifier in the error response indicates the last notification that was successfully sent.
        // Any notifications you sent after it have been discarded and must be resent. When you receive this status code, stop using this connection and open a new connection.
        if ($response && $response['status_code'] != self::SUCCESS_CODE) {
          // If error code received, re-open connection.
          $this->closeConnection();
          if ($response['status_code'] == self::SHUTDOWN_CODE) {
            // Retry all messages right after last successful.
            if (!empty($response['identifier'])) {
              $last_success_pos = array_search($response['identifier'], $indexes);
              // But if successful message is not found in recipients – repeat current.
              if ($last_success_pos !== FALSE) {
                $unprocessed_recipients = array_slice($recipients, $last_success_pos);
              }
              $last_id = $response['identifier'];
              if (function_exists('dsm')) {
                dsm('1.');
                dsm($last_id);
              }
            }
            // If id is empty we are failed on first item and should re-send all set.
            else {
              $unprocessed_recipients = $recipients;
            }
          }
          // If we on error on have no id – we don't know from where to start that's...
          elseif (!empty($response['identifier'])) {
            $malformed_pos = array_search($response['identifier'], $indexes);
            if ($malformed_pos !== FALSE) {
              // Store error within failed recipient.
              $recipients[$indexes[$malformed_pos]]->error_code = $response['status_code'];
              // Start again from next recipient.
              $unprocessed_recipients = array_slice($recipients, $malformed_pos + 1);
            }
            $last_id = $response['identifier'];
            if (function_exists('dsm')) {
              dsm('2.');
              dsm($last_id);
            }
          }
        }
      }

      $this->closeConnection();
    } catch (PushSenderConnectionException $e) {
      // Don't touch connection if it was failed.
      $result['success'] = FALSE;
      $result['message'] = $e->getMessage();
    } catch (\Exception $e) {
      $this->closeConnection();
      $result['success'] = FALSE;
      $result['message'] = $e->getMessage();
    }

    $result['stats']['time_lapsed'] = timer_read('send_push_message');
    $result['stats']['memory'] = memory_get_peak_usage();
    $result['stats']['messages_sent'] = $messages_sent;
    $result['stats']['messages_prepared'] = $messages_prepared;
    $result['stats']['reconnection_count'] = $reconnection_count;

    return $result;
  }
}
