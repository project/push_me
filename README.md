# Module info

Optional integration with drushd module. It's preferred way for high-volume schedules.
Optional feature to distribute messages among registered time (immediate/hourly modes).
Optional integration with admin_views. You may enable admin_views module and include 
bulk operation 'Send push message to the selected users' into views actions.

# Installation

## Put certificates
TODO: Define where to reside.
TODO: Move all settings to UI.

## Set up process mode

$conf['push_me']['process_mode'] = 'cron'; // or 'daemon'

In case of daemon, make sure your background worker is up and running.

## Set up certificates paths and development mode

Settings need to be set up:

$conf['push_me']['env_key'] = 'prod'; // or 'dev'

// Store all values in single array to make possible to switch to dev values even on prod site.
$conf['push_me']['apns']['certificates'] = array(
  'default' => array(
    'dev' => 'apns-development-6747204111.pem',
    'prod' => 'apns-production-6747204111.pem',
  ),
  'siis' => array(
    'dev' => 'apns-development-siis.pem',
    'prod' => 'apns-production-siis.pem',
  ),
  'test2' => array(
    'dev' => 'apns-development-test2.pem',
    'prod' => 'apns-production-test2.pem',
  ),
);

$conf['push_me']['apns']['host']['dev'] = 'gateway.sandbox.push.apple.com';
$conf['push_me']['apns']['host']['prod'] = 'gateway.push.apple.com';


## Set up GCM API key

$conf['push_me']['gcm']['api_key'] = 'XXXXX';

## Set up background worker to process tasks via cron

First we stop daemon if it's currently running and re-run it.

drush daemon stop push_me_worker
drush daemon start push_me_worker
