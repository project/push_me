<?php

/**
 * @file Includes any default rules integration provided by the module.
 */


/**
 * Implements hook_default_rules_configuration().
 */
function push_me_default_rules_configuration() {
  $configs['rules_send_push_on_update'] = rules_import('{ "rules_send_push_on_update" : {
    "LABEL" : "Send push on update",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "OWNER" : "rules",
    "REQUIRES" : [ "push_me", "rules" ],
    "ON" : { "node_insert" : [], "node_update" : [] },
    "IF" : [ { "push_me_push_notify_is_checked" : { "node" : [ "node" ] } } ],
    "DO" : [
      { "push_me_schedule_task" : { "message" : "[node:title]", "mode" : "immediate" } }
    ]
  }
}');

  return $configs;
}
