<?php

/**
 * @file
 * Action for send push message to users.
 */

use Drupal\push_me\PushProviderManager;

/**
 * Implements hook_action_info().
 */
function push_me_action_info() {
  $action['push_me_send_push_message'] = [
    'label' => t('Send push message to the selected users'),
    'type' => 'user',
    'aggregate' => TRUE,
    'configurable' => TRUE,
    'triggers' => ['any'],
  ];

  return $action;
}

/**
 * Form builder for push message action.
 */
function push_me_send_push_message_form($context) {
  require_once('push_me.admin.inc');

  $form = push_me_send_push_form([], []);
  unset($form['submit']);
  unset($form['advanced']['token']);
  unset($form['advanced']['env_key']);
  unset($form['advanced']['certificate_set']);

  return $form;
}

/**
 * Validate for push message action form.
 */
function push_me_send_push_message_validate($form, &$form_state) {
  if (empty($form_state['values']['message']) && empty($form_state['values']['payload'])) {
    form_set_error('message', t('Message or payload is required'));
  }
  if (!empty($form_state['values']['extra']) && !push_me_validate_json($form_state['values']['extra'])) {
    form_set_error('extra', t('Extra must be a valid JSON'));
  }
  if (!empty($form_state['values']['payload']) && !push_me_validate_json($form_state['values']['payload'])) {
    form_set_error('payload', t('Payload must be a valid JSON'));
  }
}

/**
 * Submit for push message action form.
 */
function push_me_send_push_message_submit($form, &$form_state) {
  $message = [
    'text' => $form_state['values']['message'],
    'extra' => !empty($form_state['values']['extra'])
      ? drupal_json_decode($form_state['values']['extra'])
      : [],
    'payload' => !empty($form_state['values']['payload'])
      ? drupal_json_decode($form_state['values']['payload'])
      : [],
    'settings' => [],
  ];
  $providers = !empty($form_state['values']['provider'])
    ? [$form_state['values']['provider']]
    : PushProviderManager::getProvidersList();
  $task_mode = $form_state['values']['task_mode'];


  return [
    'message' => $message,
    'providers' => $providers,
    'task_mode' => $task_mode,
  ];
}

/**
 * Action callback for views bulk operation.
 */
function push_me_send_push_message($users, $context) {
  $uids = [];
  foreach ($users as $user) {
    $uids[] = $user->uid;
  }

  $devices = _user_device_load_multiple(['uid' => $uids]);
  foreach ($context['providers'] as $provider) {
    $os_tokens = [];
    foreach ($devices as $device) {
      if ($device->push_provider == $provider) {
        $os_tokens[] = $device->push_token;
      }
    }
    if (!empty($os_tokens)) {
      push_me_send_message($context['message'], $context['task_mode'], [$provider], $os_tokens);
    }
  }
}

/**
 * Validate JSON.
 */
function push_me_validate_json($string) {
  json_decode($string);

  return (json_last_error() == JSON_ERROR_NONE);
}
